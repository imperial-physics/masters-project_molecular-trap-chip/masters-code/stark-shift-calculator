(* ::Package:: *)

BeginPackage["eigenOps`"]

BasisDimensionality::usage = "BasisDimensionality[J_] returns the number of basis states of Hamiltonian up to J"
GetState::usage = "GetState[list, J, M] returns the list corresponding to the state |J,M> in the given list"
EnergyExpectation::usage ="EnergyExpectation[vector_, hamiltonian_] returns the energy of vector under given hamiltonian"
SortEigenvectors::usage = "SortEigenvectors[newEigen, refEigen] sorts a list of eigenvectors newEigen according to a reference list refEigen.
                            This is done by placing the new eigenvectors at the position of the reference eigenvector with which it has the greatest overlap"
AddNEnergies::usage = "AddNEnergies[Hamiltonian,variable,lambdaRange] creates a list of eigenenergies for a Hamiltonian with an increasing perturbation in the given variable over the given lambdaRange"
Get\[Lambda]EigenEnergyList::usage = "Get\[Lambda]EigenEnergyList[Jmax,\[Lambda]max,\[Lambda]step] returns list of \[Lambda] and eigenenergies for eigenstates of a given set determined by Jmax"


Begin["`Private`"]
Needs["HBuilding`"]

BasisDimensionality[J_] := If[J==0, 1, (2J+1)+BasisDimensionality[J-1]]
IndexofState[J_,M_]:= Total[(2# +1) &/@Range[0,J-1]] +(J+M)+1
GetState[list_, J_,M_]:= list[[IndexofState[J,M]]]

EnergyExpectation[vector_, hamiltonian_]:=Module[{normaliseVector, energy},
normaliseVector = Normalize[vector];
(*E_v = <v|H|v>*)
energy = Dot[normaliseVector,Dot[hamiltonian, normaliseVector]];
Re[energy]
]

(*Returns the index of the vector in oldEigenset which newEigen has the greatest overlap with*)
MatchtoReference[newEigen_, oldEigenset_]:=Module[{overlap},
(*overlap of testEigenvector with each in testSet*)
overlap=Abs[Dot[newEigen, #]] &/@ oldEigenset;
overlap = N[Re[overlap]];
(*Finds corresponding vector in testSet*)
Ordering[overlap,-1][[1]]
]

(*Creates a mapping *)
FindMap [newEigen_, oldEigen_]:=Module[{newIndices, mapping},
(*creates a mapping from oldEigen to newEigen*)
newIndices = MatchtoReference[#, oldEigen] &/@ newEigen;
(*inverts the mapping*)
Position[newIndices, #][[1]][[1]] &/@ Range[Length[newEigen]]
]

(*Applies the mapping from FindMap to order the given set of eigenstates in the conventional manner*)
SortEigenvectors[newEigen_, refEigen_]:= newEigen[[FindMap[newEigen,refEigen]]]

(*Appends the energies for each eigenstate for a given hamiltonian to a set of lists of energies for each eigenstate*)
AddNewEnergy[newEigen_, energiesList_, Hamiltonian_]:=Append[energiesList[[#]], EnergyExpectation[newEigen[[#]],Hamiltonian]] &/@ Range[Length[newEigen]]

AddNEnergies[Hamiltonian_, variable_,lambdaRange_]:=Module[{basisDimensions, numericH, newEigen, refEigen, energiesList, i},
basisDimensions = Length[Hamiltonian];
(*Constructs a list of eigenvectors for the unperturbed diagonal hamiltonian*)
refEigen = ReplacePart[Table[0,basisDimensions], #->1] &/@ Range[basisDimensions];
(*Constructs a list of empty lists to be populated with eigenstate energies*)
energiesList = {} &/@ refEigen;

(*Loops through increasing values of \[Lambda] constructing and diagonalising the hamiltonian.
  New eigenenergies are found and appended to energiesList and newly constructed eigenvectors are used as reference for sorting*)
For[i=1, i<=Length[lambdaRange], i++,
numericH =Hamiltonian /. variable->lambdaRange[[i]];
newEigen = N[Eigenvectors[numericH,Cubics->True, Quartics->True]];
newEigen = Normalize[#] &/@newEigen;
newEigen = SortEigenvectors[newEigen, refEigen];
energiesList = AddNewEnergy[newEigen, energiesList, numericH];
refEigen = newEigen;
]
(*Eliminates Nulls from generated list (unknown why these occur)*)
energiesList /.Null->Sequence[]
]

Get\[Lambda]EigenEnergyList[Jmax_,\[Lambda]max_,\[Lambda]step_]:=Module[{HMat,lambdaRange},
HMat = BuildMatrix[Jmax] /. {HBuilding`Private`\[Lambda] ->\[Lambda]};
HMat //MatrixForm;
lambdaRange = Range[0, \[Lambda]max, \[Lambda]step];
{lambdaRange,AddNEnergies[HMat,\[Lambda],lambdaRange]}
]

End[]
EndPackage[]
