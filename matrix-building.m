(* ::Package:: *)

BeginPackage["HBuilding`"]


BuildMatrix::usage = "BuildMatrix[J, M] returns a matrix of the Hamiltonian in the |J, M> basis"
MatrixElement::usage = "MatrixElement[j1, m1, j2,m2] returns the matrix element <J1,M1|H|J2,M2>"
BuildList::usage = "BuildList[J, M] returns a list of the |J, M> basis states, grouped into lists of state with equal J"

Begin["`Private`"]

BuildMatrix[J_]:= Module[{tempList, tempMatrix,f},
tempList = BuildList[J];
(*Constructs a lower triangular "matrix" where each element is a 2x2 matrix of consisting of {|J1,M1>,|J2,M2>}*)
tempMatrix = tempList[[#]][[1;;#]] & /@ Range[Length[tempList]];
(*Replaces the matrix elements with <J1,M1|H|J2,M2> and pads upper triangle with zeros*)
tempMatrix = PadRight[(MatrixElement @@ Flatten[#] &/@#)&/@ tempMatrix, Table[Length[Last[tempList]],2]];
(*Matrix should be symmetric so matrix is reflected along diagonal to populate upper triangle*)
tempMatrix + UpperTriangularize[Transpose[tempMatrix],1]
]

BuildList[J_]:= Module[{tempList,f},
tempList =Table[{#,m},{m,-#, #}]&/@Range[0,J];
tempList =Flatten[tempList,1];
f[x_]:={tempList[[x]], #} &/@ tempList;
tempList =f[#] &/@ Range[Length[tempList]]
]

MatrixElement[j1_, m1_, j2_,m2_]:= RigidRotorEnergy[j1, m1, j2,m2]+\[Lambda]*CosTerm[j1, m1, j2,m2]
RigidRotorEnergy[j1_, m1_, j2_,m2_]:= j1(j1+1)KroneckerDelta[j1,j2]KroneckerDelta[m1,m2]
CosTerm[j1_, m1_, j2_,m2_]:=(-1)^m2*Sqrt[(2j1+1)(2j2+1)]*ThreeJElement[j1, -m1, j2,m2]*ThreeJElement[j1, 0, j2,0]

(*Convenience function to prevent having to add central column of 3j symbol everytime*)
ThreeJElement[j1_, m1_, j2_,m2_]:=Quiet[ThreeJSymbol[{j1,m1},{1,0},{j2,m2}]]

End[]
EndPackage[]
