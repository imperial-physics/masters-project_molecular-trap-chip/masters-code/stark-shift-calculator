(* ::Package:: *)

BeginPackage["wAnalysis`"]

PlotW::usage = "PlotW[\[Lambda], W] plots energy over a range of values of \[Lambda]"
FindMaxima::usage = "FindMaxima[energyList] returns {x,y} coordinates for position of peaks/maxima"
EnergyDiff21::usage = "EnergyDiff21[energyList2,energyList1] returns energy difference between two lists of energies corresponding to selected states"
Interpolate::usage = "InterpolateEnergy[\[Lambda]List,energyList] returns interpolating function for specified energy list"
FindSweetSpot::usage = "FindSweetSpot[\[Lambda]List,energyList2,energyList1] returns {x,y} coordinates for postion of sweet spot"
PlotEnergyDiff21::usage = "PlotEnergyDiff21[\[Lambda]List,energyList2,energyList1] returns plot of energy difference between |N=2,m=0> and |N=1,m=0> states with vertical line corresponding to position of sweet spot"
DimensionaliseField::usage = "DimensionaliseField[field,B,\[Mu]] dimensionalises electric field; B and \[Mu] values for CaF are used by default"
DimensionaliseEnergy::usage = "DimensionaliseEnergy[energy,B] dimensionalises energy; B values for CaF are used by default"
Dimensionalise::usage = "Dimensionalise[fieldEnergyCoords,B,\[Mu]] dimensionalises electric field and energy given dimensionless coords { \[Mu]E/B , W/B }; B and \[Mu] values for CaF are used by default" 
ErrorAnal::usage = "ErrorAnal[testJmax,refJmax,\[Lambda]max,\[Lambda]step] returns maximum percentage error of eigenenergies between reference and test set for |1,0> state and |2,0> state"


Begin["`Private`"]
Needs["HBuilding`"]
Needs["eigenOps`"]

(*Defining Constants*)
planckConstant=6.62607004*10^(-34);
BValue=10.2675*10^9; (*in Hz*)
BEnergy=BValue*planckConstant; (*In J*)
\[Mu]Value=3.06/(2.9979*10^(29)) (*3.06 Debye to SI units*)


PlotW[\[Lambda]_, WSet_]:= Module[{dataset},
dataset = Labeled[Transpose[{\[Lambda],#}], First[Position[WSet,#]]] &/@ WSet;
ListPlot[dataset, {Joined-> True, AxesLabel->{"\!\(\*FractionBox[\(\[Mu]E\), \(B\)]\)","\!\(\*FractionBox[\(W\), \(B\)]\)"}}]
]

FindMaxima[energyList_,\[Lambda]step_]:=Module[{peaks},
peaks=FindPeaks[energyList,0,InterpolationOrder->3];
{#[[1]]*\[Lambda]step,#[[2]]}&/@peaks
]
(* to plot maximum points in red
ListLinePlot[values,Epilog\[Rule]{Red,PointSize[0.04],Point[peaks]}] *)

EnergyDiff21[energyList2_,energyList1_]:=energyList2-energyList1

Interpolate[\[Lambda]List_,energyList_]:=Module[{\[Lambda]energyList},
(*Construct {\[Lambda],energy} list for interpolation*)
\[Lambda]energyList=Transpose[{\[Lambda]List,energyList}];
Interpolation[\[Lambda]energyList]
]

FindSweetSpot[\[Lambda]List_,energyList2_,energyList1_]:=Module[{\[CapitalDelta]w21List,\[CapitalDelta]w21Func,sweetSpotCoords},
\[CapitalDelta]w21List=EnergyDiff21[energyList2,energyList1];
(*Finding minimum corresponds to d\[CapitalDelta]w12/d\[Lambda]=0 (Sweetspot)*)
\[CapitalDelta]w21Func=Interpolate[\[Lambda]List,\[CapitalDelta]w21List];
(*Find minimum of interpolation function*)
sweetSpotCoords=Quiet[NMinimize[{\[CapitalDelta]w21Func[lambda],0<lambda<\[Lambda]List[[-1]]},lambda]]; (*output of NMinimize is {\[CapitalDelta]w21min,\[Lambda]\[Rule]\[Lambda]min}*)
{lambda/.sweetSpotCoords[[2]],sweetSpotCoords[[1]]}
]

PlotEnergyDiff21[\[Lambda]List_,energyList2_,energyList1_]:=Module[{\[CapitalDelta]w21List,sweetSpot,\[Lambda]\[CapitalDelta]w21List},
(*Constructs lists of energy differences and of values of \[Lambda] used*)
\[CapitalDelta]w21List=EnergyDiff21[energyList2,energyList1];
sweetSpot=FindSweetSpot[\[Lambda]List,energyList2,energyList1];
\[Lambda]\[CapitalDelta]w21List=Transpose[{\[Lambda]List,\[CapitalDelta]w21List}];
(*Plots energy difference curve and places a vertical line at the sweet spot*)
ListPlot[\[Lambda]\[CapitalDelta]w21List,AxesLabel->{"\!\(\*FractionBox[\(\[Mu]E\), \(B\)]\)","\!\(\*FractionBox[SubscriptBox[\(\[CapitalDelta]w\), \(21\)], \(B\)]\)"},
Joined->True,InterpolationOrder->2,Mesh->All,Epilog->Line[{{sweetSpot[[1]],0},{sweetSpot[[1]],sweetSpot[[2]]}}]]
]

DimensionaliseField[field_,B_:BEnergy,\[Mu]_:\[Mu]Value]:=field*B/\[Mu]

DimensionaliseEnergy[energy_,B_:BEnergy]:=energy*B

Dimensionalise[fieldEnergyCoords_,B_:BEnergy,\[Mu]_:\[Mu]Value]:=Module[{field,energy},
field=fieldEnergyCoords[[1]];
energy=fieldEnergyCoords[[2]];
{DimensionaliseField[field,B,\[Mu]],DimensionaliseEnergy[energy,B]}
]
(*
B=10.2675*10^9*6.62607004*10^(-34) (* in units of energy kgm^2s^-2 *)
\[Mu]=3.06/(2.9979*10^(29)) (*in units of Cm where 1C=1As^(-1) *)
						(* 1Cm = 2.9979*10^29D *)
*)

PercentageError[refEnergy_,testEnergy_]:=Abs[((testEnergy-refEnergy)/refEnergy)*100]

ErrorAnal[testJmax_,refJmax_,\[Lambda]max_,\[Lambda]step_]:=Module[{refEigenenergies,testEigenenergies,refEigenenergies10,refEigenenergies20,testEigenenergies10,testEigenenergies20,range,max10Error,max20Error},
refEigenenergies = Get\[Lambda]EigenEnergyList[refJmax,\[Lambda]max,\[Lambda]step][[2]];
testEigenenergies = Get\[Lambda]EigenEnergyList[testJmax,\[Lambda]max,\[Lambda]step][[2]];
refEigenenergies10 = GetState[refEigenenergies,1,0];
refEigenenergies20 = GetState[refEigenenergies,2,0];
testEigenenergies10 = GetState[testEigenenergies,1,0];
testEigenenergies20 = GetState[testEigenenergies,2,0];
range=Range[Length[refEigenenergies10]];
max10Error=Max[PercentageError[refEigenenergies10[[#]],testEigenenergies10[[#]]]&/@range];
max20Error=Max[PercentageError[refEigenenergies20[[#]],testEigenenergies20[[#]]]&/@range];
{max10Error,max20Error}
]

End[]
EndPackage[]
